#include <IRremote.h>
#include "SR04.h"


unsigned char start01[] = {0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80,0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};
unsigned char front[] = {0x00,0x00,0x00,0x00,0x00,0x24,0x12,0x09,0x12,0x24,0x00,0x00,0x00,0x00,0x00,0x00};
unsigned char back[] = {0x00,0x00,0x00,0x00,0x00,0x24,0x48,0x90,0x48,0x24,0x00,0x00,0x00,0x00,0x00,0x00};
unsigned char left[] = {0x00,0x00,0x00,0x00,0x00,0x00,0x44,0x28,0x10,0x44,0x28,0x10,0x44,0x28,0x10,0x00};
unsigned char right[] = {0x00,0x10,0x28,0x44,0x10,0x28,0x44,0x10,0x28,0x44,0x00,0x00,0x00,0x00,0x00,0x00};
unsigned char STOP01[] = {0x2E,0x2A,0x3A,0x00,0x02,0x3E,0x02,0x00,0x3E,0x22,0x3E,0x00,0x3E,0x0A,0x0E,0x00};
unsigned char clear[] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};



#define SCL_Pin  A5
#define SDA_Pin  A4

#define TRIG_PIN 12
#define ECHO_PIN 13


SR04 sr04 = SR04(ECHO_PIN,TRIG_PIN);

long distance;
long distance1;
long distance2;
long distance3;



const int ML_Ctrl = 4;
const int ML_PWM = 5;
const int MR_Ctrl = 2;
const int MR_PWM = 9;


const int sensor_l = 6;
const int sensor_c = 7;
const int sensor_r = 8;
const int servopin = 10;

int l_val;
int c_val;
int r_val;


char command_val;


int RECV_PIN = A3;

IRrecv irrecv(RECV_PIN);
long irr_val;

decode_results results;

//*******************************************************
void setup() {

  Serial.begin(9600);
  
  
  
  servopulse(servopin,90);
  delay(300);
  pinMode(ML_Ctrl,OUTPUT);
  pinMode(ML_PWM,OUTPUT);
  pinMode(MR_Ctrl,OUTPUT);
  pinMode(MR_PWM,OUTPUT);
  pinMode(sensor_l,INPUT);
  pinMode(sensor_c,INPUT);
  pinMode(sensor_r,INPUT);
  pinMode(SCL_Pin,OUTPUT);
  pinMode(SDA_Pin,OUTPUT);
  

  //Clear the screen
  matrix_display(clear);
  matrix_display(start01);
  irrecv.enableIRIn(); 
}
//*******************************************************

void loop() {

  if(Serial.available()>0 || irrecv.decode(&results))
  {
    irr_val = results.value;
    Serial.println(irr_val, HEX);
    command_val = Serial.read();
    Serial.println(command_val);

    switch(irr_val)
    {
      case 0xFF629D : car_front(); matrix_display(clear); matrix_display(front); break;
      case 0xFFA857 : car_back(); matrix_display(clear); matrix_display(back); break;
      case 0xFF22DD : car_left(); matrix_display(clear); matrix_display(left); break;
      case 0xFFC23D : car_right(); matrix_display(clear); matrix_display(right); break;
      case 0xFF02FD : car_Stop(); matrix_display(clear); matrix_display(STOP01); break;
      case 0xFF6897 : tracking(); break;
      case 0xFF9867 : avoid(); break;
      case 0xFFB04F : follow_car(); break;
    }
    irrecv.resume(); 
  }
  switch(command_val)
  {
    case 'F': car_front(); matrix_display(front); break;
    case 'B': car_back(); matrix_display(back); break;
    case 'L': car_left(); matrix_display(left); break;
    case 'R': car_right(); matrix_display(right); break;
    case 'S': car_Stop(); matrix_display(STOP01); break;
    case 'X': tracking(); break;
    case 'Y': avoid();break;
    case 'U': follow_car();break;
  }
}


//*******************************************************
void car_front()
{
  digitalWrite(ML_Ctrl,HIGH);
  analogWrite(ML_PWM,200);
  digitalWrite(MR_Ctrl,HIGH);
  analogWrite(MR_PWM,200);
}
void car_back()
{
  digitalWrite(ML_Ctrl,LOW);
  analogWrite(ML_PWM,200);
  digitalWrite(MR_Ctrl,LOW);
  analogWrite(MR_PWM,200);
}
void car_left()
{
  digitalWrite(ML_Ctrl,LOW);
  analogWrite(ML_PWM,200);
  digitalWrite(MR_Ctrl,HIGH);
  analogWrite(MR_PWM,200);
}
void car_right()
{
  digitalWrite(ML_Ctrl,HIGH);
  analogWrite(ML_PWM,200);
  digitalWrite(MR_Ctrl,LOW);
  analogWrite(MR_PWM,200);
}
void car_Stop()
{
  analogWrite(ML_PWM,0);
  analogWrite(MR_PWM,0);
}
//*******************************************************

void avoid()
{
  matrix_display(start01);
  int track_flag = 0;
  while(track_flag == 0)
  {
    distance1=sr04.Distance();
    if((distance1 < 20)&&(distance1 != 0))
    {
      Stop2_LT();
      delay(100);
      servopulse(servopin,180);
      delay(500);
      distance2=sr04.Distance();
      delay(100);
      servopulse(servopin,0);
      delay(500);
      distance3=sr04.Distance();
      delay(100);
        if(distance2 > distance3)
      {
        car_left();
        servopulse(servopin,90);
      }
      else
      {
        car_right();
        servopulse(servopin,90);
      }
    }
    else
    {
      car_front();
    }
    if(Serial.available()>0 || irrecv.decode(&results))
    {
      irr_val = results.value;
      Serial.println(irr_val, HEX);
      command_val = Serial.read();
      if(command_val == 'S' || irr_val == 0xFF02FD)
      {
        servopulse(servopin,90);
        track_flag = 1;
      }
      irrecv.resume();
    }
  }
}
//*******************************************************

void follow_car()
{
  matrix_display(start01);
  servopulse(servopin,90);
  int track_flag = 0;
  while(track_flag == 0)
  {
    distance = sr04.Distance();
  
    if(distance<8)
    {
      car_back();
    }
    else if((distance>=8)&&(distance<13))
    {
      car_Stop();
    }
    else if((distance>=13)&&(distance<35))
    {
      car_front();
    }
    else
    {
      car_Stop();
    }
        if(Serial.available()>0 || irrecv.decode(&results))
    {
      irr_val = results.value;
      Serial.println(irr_val, HEX);
      command_val = Serial.read();
      if(command_val == 'S' || irr_val == 0xFF02FD)
      {
        track_flag = 1;
      }
      irrecv.resume();
    }
  }
}
//*******************************************************

void servopulse(int servopin,int myangle)
{
  for(int i=0;i<30;i++){
    int pulsewidth = (myangle*11)+500;
    digitalWrite(servopin,HIGH);
    delayMicroseconds(pulsewidth);
    digitalWrite(servopin,LOW);
    delay(20-pulsewidth/1000);
  }
}
//*******************************************************

void tracking()
{
  matrix_display(start01);
  int track_flag = 0;
  while(track_flag == 0)
  {
    l_val = digitalRead(sensor_l);
    c_val = digitalRead(sensor_c);
    r_val = digitalRead(sensor_r);
  
    if(c_val == 1)
    {
      front2_LT();
    }
    else
    {
      if((l_val == 1)&&(r_val == 0))
      {
        left_LT();
      }
      else if((l_val == 0)&&(r_val == 1))
      {
        right_LT();
      }
      else
      {
        Stop_LT();
      }
    }
    if(Serial.available()>0 || irrecv.decode(&results))
    {
      irr_val = results.value;
      Serial.println(irr_val, HEX);
      command_val = Serial.read();
      if(command_val == 'S' || irr_val == 0xFF02FD)
      {
        track_flag = 1;
      }
      irrecv.resume();
    }
  }
}
//*******************************************************
void front_LT()
{
  digitalWrite(ML_Ctrl,HIGH);
  analogWrite(ML_PWM,220);
  digitalWrite(MR_Ctrl,HIGH);
  analogWrite(MR_PWM,190);
}
void front2_LT()
{
  digitalWrite(ML_Ctrl,HIGH);
  analogWrite(ML_PWM,75);
  digitalWrite(MR_Ctrl,HIGH);
  analogWrite(MR_PWM,70);
}

void back_LT()
{
  digitalWrite(ML_Ctrl,LOW);
  analogWrite(ML_PWM,220);
  digitalWrite(MR_Ctrl,LOW);
  analogWrite(MR_PWM,190);
}
void back2_LT()
{
  digitalWrite(ML_Ctrl,LOW);
  analogWrite(ML_PWM,110);
  digitalWrite(MR_Ctrl,LOW);
  analogWrite(MR_PWM,90);
}

void left_LT()
{
  digitalWrite(ML_Ctrl,LOW);
  analogWrite(ML_PWM,220);
  digitalWrite(MR_Ctrl,HIGH);
  analogWrite(MR_PWM,190);
}
void right_LT()
{
  digitalWrite(ML_Ctrl,HIGH);
  analogWrite(ML_PWM,220);
  digitalWrite(MR_Ctrl,LOW);
  analogWrite(MR_PWM,190);
}
void Stop_LT()
{
  analogWrite(ML_PWM,0);
  analogWrite(MR_PWM,0);
}
void Stop2_LT()
{
  digitalWrite(ML_Ctrl,LOW);
  analogWrite(ML_PWM,200);
  digitalWrite(MR_Ctrl,LOW);
  analogWrite(MR_PWM,200);
  delay(50);
  analogWrite(ML_PWM,0);
  analogWrite(MR_PWM,0);
}
//*******************************************************

void matrix_display(unsigned char matrix_value[])
{
  IIC_start();  
  IIC_send(0xc0);  
    for(int i = 0;i < 16;i++) 
  {
     IIC_send(matrix_value[i]); 
  }
  IIC_end();   
  IIC_start();
  IIC_send(0x8A);  
  IIC_end();
}
//*******************************************************

void IIC_start()
{
  digitalWrite(SCL_Pin,HIGH);
  delayMicroseconds(3);
  digitalWrite(SDA_Pin,HIGH);
  delayMicroseconds(3);
  digitalWrite(SDA_Pin,LOW);
  delayMicroseconds(3);
}
//*******************************************************

void IIC_send(unsigned char send_data)
{
  for(char i = 0;i < 8;i++)  
  {
      digitalWrite(SCL_Pin,LOW);  
      delayMicroseconds(3);
      if(send_data & 0x01)  
      {
        digitalWrite(SDA_Pin,HIGH);
      }
      else
      {
        digitalWrite(SDA_Pin,LOW);
      }
      delayMicroseconds(3);
      digitalWrite(SCL_Pin,HIGH); 
      send_data = send_data >> 1;  
  }
}
//*******************************************************

void IIC_end()
{
  digitalWrite(SCL_Pin,LOW);
  delayMicroseconds(3);
  digitalWrite(SDA_Pin,LOW);
  delayMicroseconds(3);
  digitalWrite(SCL_Pin,HIGH);
  delayMicroseconds(3);
  digitalWrite(SDA_Pin,HIGH);
  delayMicroseconds(3);
}
